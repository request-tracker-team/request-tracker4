#!/bin/bash
#
# Run first: apt-get install ckbuilder

set -e

THIRD_PARTY_DIR=third-party-source/devel/third-party
SRC=$THIRD_PARTY_DIR/ckeditor-src
SKINSRC=$THIRD_PARTY_DIR/flat-src
SCAYTSRC=$THIRD_PARTY_DIR/scayt-src
WSCSRC=$THIRD_PARTY_DIR/wsc-src
DEST=ckeditor-build
FINALDEST=share/static/RichText
BUILD_CONFIG=$THIRD_PARTY_DIR/ckeditor-4.5.3/build-config.js

rm -rf $DEST

## Copy aux sources into main source

mkdir -p $SRC/skins
cp -a $SKINSRC/ $SRC/skins/flat
cp -a $SCAYTSRC/ $SRC/plugins/scayt
cp -a $WSCSRC/ $SRC/plugins/wsc

# --add-exports is from https://github.com/ckeditor/ckbuilder/issues/34
java --add-exports java.desktop/sun.java2d=ALL-UNNAMED -jar /usr/bin/ckbuilder \
    --build $SRC $DEST --skip-omitted-in-build --build-config $BUILD_CONFIG

(
    cd $DEST/ckeditor
    rm -rf .github CHANGES.md LICENSE.md README.md samples
    mv ckeditor.js ckeditor.min.js
)

## Copy results into final location

# Preserve customised config file
cp -p $FINALDEST/{config.js,contents.css} $DEST/ckeditor/
rm -rf $FINALDEST
cp -a $DEST/ckeditor/ $FINALDEST/
rm -rf $DEST

rm -rf $SRC/skins/flat $SRC/plugins/scayt $SRC/plugins/wsc
