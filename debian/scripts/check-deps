#! /usr/bin/perl
#
# check-deps -- verify that debian/deps matches the output from
#   rt-test-dependencies

# Return codes:
#   0 = okay
#   1 = module in debian/deps not required by RT
#   2 = version specified in debian/deps too low or unspecified
#   4 = module required by RT absent from debian/deps
#
# These are ORed together. Suggest aborting package build if check-deps
# returns 2 or more.

use strict;
use warnings;

my %test;

sub packageName {
    $_ = shift;
    s/^/lib/;
    s/$/-perl/;
    s/::/-/g;
    lc;
}

sub verHigher {
    return $_[0] > $_[1];
}

sub check {
    my ($pkg, $ver) = @_;
    print "Checking $pkg".($ver?" ($ver)":''). " ... ";
    if (defined $test{$pkg}) {
	$_ = $test{$pkg};
	delete $test{$pkg};
	if (defined $_->{'version'}) {
	    my $needs = "needs >= $_->{'version'} ";
	    if (!defined $ver) {
		print $needs. "but we don't version !\n"; return 2;
	    } elsif (verHigher($_->{'version'}, $ver)) {
		print $needs. "but we say $ver !\n"; return 2
	    } else {
		print "okay\n"; return 0;
	    }
	} else {
	    print "okay\n"; return 0;
	}
    } else {
	print "not required (?)\n"; return 1;
    }
}

sub depline {
    my $ret = 0;
    $_ = shift;
    return 0 if /^\w+:\w+$/; # perllib:Depends etc.
    my ($pkg, $ver);
    if (/^([^\s#]+)(\s+\((.*)\))?/) {
	($pkg, $ver) = ($1, $3);
	$ver =~ s/>=\s+// if defined $ver;
    }
    if (/#\s+(=)\s+(\S*)(\s+\((.*)\))?\s*/) {
	my ($op, $mod, $xver) = ($1, $2, $4);
	# print "Override ($op) ($mod) ". ($xver?$xver:'') . "\n";
	$ver = $xver if (defined $xver);
	# Overrides (=) and extras (+)
	my @mods = split /,/, $mod;
	for (@mods) {
	    $ret |= check(packageName($_), $ver);
	}
	return 0;
    }
    return 0 unless $pkg;
    $ret |= check($pkg, $ver);
    return $ret;
}

my @components = qw(mysql postgresql modperl1);

my $cmdline = 'perl sbin/rt-test-dependencies ' .
    (join ' ', map({"--with-".$_} @components))
  || die "Can't run rt-test-dependencies";

open(TEST, $cmdline."|");

while (<TEST>) {
    if (/\s+(\S+)\s+([\d.]+)?\.\.\.(\S+)/) {
	local %_;
	($_{'module'}, $_{'version'}, $_{'result'}) = ($1, $2, $3);
	my $pkg = packageName($_{'module'});
#	print "($pkg) ($module) (" . ($version?$version:'') . ") ($result)\n";
	$test{$pkg} = \%_;
    }
}

close TEST;

my $ret;

my @depfiles = qw!debian/deps debian/rt4-clients.deps!;
my $depfile;
 
foreach $depfile (@depfiles) {
    open(DEPS, $depfile) || die "Can't open $depfile";
    while(<DEPS>) {
	$ret |= depline($_);
	
    }
}

while ((my ($k, $ref)) = each %test) {
    $ret |= 1;
    print "Need $ref->{'module'} -- ".
      $k.($ref->{'version'} ? " (>= $ref->{'version'})" : '')." !\n";
}

exit $ret;
