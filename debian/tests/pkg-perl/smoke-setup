#!/bin/sh

# Remove the dummy module created by smoke, as the tests do sanity checks
# and this one fails.
rm $TDIR/lib/Debian/pkgperl/Foobar.pm

# This is an upstream check that content meets their policies, we test this
# during builds, and it expects all the scripts to be in directories next to
# the tests. Skip it.
rm $TDIR/t/99-policy.t

# RT wants various directories and scripts next to the tests, make sure
# they're present.
mkdir $TDIR/bin
mkdir $TDIR/sbin
mkdir $TDIR/share

ln -s /usr/share/request-tracker4/etc $TDIR

for x in fonts html po static; do
  ln -s /usr/share/request-tracker4/$x $TDIR/share
done

for x in rt-externalize-attachments; do
  ln -s /usr/sbin/$x-4 $TDIR/sbin/$x
done

# rt-server isn't in /usr/sbin which is what $RT::SbinPath points to, so
# look for it in the right place. Ideally we'd install RT::Test knowing where
# to find rt-server, but this is the first time this has been an issue, so,
# I won't worry about updating that, especially given it isn't a program that
# would normally be used.
#
# smoke uses prove --blib, so we can put a modified RT/Test.pm in there and
# it takes precedence.
#
mkdir -p $TDIR/blib/lib/RT
sed \
  's,\$RT::SbinPath/rt-server,/usr/share/request-tracker4/libexec/rt-server,' \
  /usr/share/request-tracker4/lib/RT/Test.pm > $TDIR/blib/lib/RT/Test.pm

# Ensure that directories that RT tries to write to are under $TDIR as we
# don't run the tests as root.
mkdir -p $TDIR/var/lib/request-tracker4
mkdir $TDIR/var/cache
sed "s,/var/lib,$TDIR/var/lib,;s,/var/cache,$TDIR/var/cache," \
  /usr/share/request-tracker4/lib/RT/Generated.pm > $TDIR/blib/lib/RT/Generated.pm

# Potentially tell prove to run tests in parallel.
if [ "x$DEB_BUILD_OPTIONS" != "x" ]; then
    jobs=$(echo $DEB_BUILD_OPTIONS | sed -E 's/^.*?parallel=([[:digit:]]*).*?$/\1/')

    if [ $jobs != $DEB_BUILD_OPTIONS ]; then 
        echo "--jobs $jobs" >> $TDIR/.proverc
    fi
fi

# Be verbose.
echo "--verbose" >> $TDIR/.proverc
