From 142ac1022545a09f96845cecf8b589da4891ff4e Mon Sep 17 00:00:00 2001
From: Andrew Ruthven <andrew@etc.gen.nz>
Date: Mon, 6 Jul 2020 21:30:54 +1200
Subject: Force use of IPv4 for LDAP test.

Net::LDAP::Server::Test binds to IPv6 by default, but Net::LDAP uses
'localhost' which resolves to an IPv4 address.  Even when I switched
the call to Net::LDAP->new() to use ip6-localhost it failed elsewhere
due to RT using 127.0.0.1.

Patch-Name: fix_test_ldap_ipv4.diff
Forwarded: https://github.com/bestpractical/rt/pull/367
---
 t/externalauth/ldap.t                 | 12 +++++++++---
 t/externalauth/ldap_email_login.t     | 11 +++++++++--
 t/externalauth/ldap_escaping.t        | 11 +++++++++--
 t/externalauth/ldap_group.t           | 11 +++++++++--
 t/externalauth/ldap_privileged.t      | 11 +++++++++--
 t/ldapimport/group-callbacks.t        | 11 +++++++++--
 t/ldapimport/group-import.t           | 11 +++++++++--
 t/ldapimport/group-member-import.t    | 11 +++++++++--
 t/ldapimport/group-rename.t           | 11 +++++++++--
 t/ldapimport/user-import-cfs.t        | 11 +++++++++--
 t/ldapimport/user-import-privileged.t | 11 +++++++++--
 t/ldapimport/user-import.t            | 11 +++++++++--
 12 files changed, 108 insertions(+), 25 deletions(-)

diff --git a/t/externalauth/ldap.t b/t/externalauth/ldap.t
index b6d696ab..396eeab8 100644
--- a/t/externalauth/ldap.t
+++ b/t/externalauth/ldap.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -7,12 +8,17 @@ eval { require RT::Authen::ExternalAuth; require Net::LDAP::Server::Test; 1; } o
     plan skip_all => 'Unable to test without Net::LDAP and Net::LDAP::Server::Test';
 };
 
-
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port" );
 
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 my $username = "testuser";
 my $base     = "dc=bestpractical,dc=com";
diff --git a/t/externalauth/ldap_email_login.t b/t/externalauth/ldap_email_login.t
index ffb726f7..69e715a7 100644
--- a/t/externalauth/ldap_email_login.t
+++ b/t/externalauth/ldap_email_login.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -8,10 +9,16 @@ eval { require RT::Authen::ExternalAuth; require Net::LDAP::Server::Test; 1; } o
 };
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port" );
 
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 
 my $base = 'dc=bestpractical,dc=com';
diff --git a/t/externalauth/ldap_escaping.t b/t/externalauth/ldap_escaping.t
index b46c3ffe..6f8b500f 100644
--- a/t/externalauth/ldap_escaping.t
+++ b/t/externalauth/ldap_escaping.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -9,10 +10,16 @@ eval { require RT::Authen::ExternalAuth; require Net::LDAP::Server::Test; 1; } o
 
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port" );
 
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 
 my $users_dn = "ou=users,dc=bestpractical,dc=com";
diff --git a/t/externalauth/ldap_group.t b/t/externalauth/ldap_group.t
index 168c37b0..3bb23ede 100644
--- a/t/externalauth/ldap_group.t
+++ b/t/externalauth/ldap_group.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 # This lets us change config during runtime without restarting
 BEGIN {
@@ -14,10 +15,16 @@ eval { require RT::Authen::ExternalAuth; require Net::LDAP::Server::Test; 1; } o
 
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port" );
 
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 
 my $users_dn = "ou=users,dc=bestpractical,dc=com";
diff --git a/t/externalauth/ldap_privileged.t b/t/externalauth/ldap_privileged.t
index 02e760bf..7e1db471 100644
--- a/t/externalauth/ldap_privileged.t
+++ b/t/externalauth/ldap_privileged.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -8,10 +9,16 @@ eval { require RT::Authen::ExternalAuth; require Net::LDAP::Server::Test; 1; } o
 };
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port" );
 
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 my $username = "testuser";
 my $base     = "dc=bestpractical,dc=com";
diff --git a/t/ldapimport/group-callbacks.t b/t/ldapimport/group-callbacks.t
index 272d3292..86901b31 100644
--- a/t/ldapimport/group-callbacks.t
+++ b/t/ldapimport/group-callbacks.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -11,9 +12,15 @@ my $importer = RT::LDAPImport->new;
 isa_ok($importer,'RT::LDAPImport');
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port");
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 $ldap->add("dc=bestpractical,dc=com");
 
diff --git a/t/ldapimport/group-import.t b/t/ldapimport/group-import.t
index fc3f97bd..dddf3c6b 100644
--- a/t/ldapimport/group-import.t
+++ b/t/ldapimport/group-import.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -11,9 +12,15 @@ my $importer = RT::LDAPImport->new;
 isa_ok($importer,'RT::LDAPImport');
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port");
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 $ldap->add("dc=bestpractical,dc=com");
 
diff --git a/t/ldapimport/group-member-import.t b/t/ldapimport/group-member-import.t
index 651f5ab6..9be83c54 100644
--- a/t/ldapimport/group-member-import.t
+++ b/t/ldapimport/group-member-import.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -11,9 +12,15 @@ my $importer = RT::LDAPImport->new;
 isa_ok($importer,'RT::LDAPImport');
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port");
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 $ldap->add("dc=bestpractical,dc=com");
 
diff --git a/t/ldapimport/group-rename.t b/t/ldapimport/group-rename.t
index 786533ef..56967620 100644
--- a/t/ldapimport/group-rename.t
+++ b/t/ldapimport/group-rename.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -11,9 +12,15 @@ my $importer = RT::LDAPImport->new;
 isa_ok($importer,'RT::LDAPImport');
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port");
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 $ldap->add("dc=bestpractical,dc=com");
 
diff --git a/t/ldapimport/user-import-cfs.t b/t/ldapimport/user-import-cfs.t
index a0c723ee..d7f47854 100644
--- a/t/ldapimport/user-import-cfs.t
+++ b/t/ldapimport/user-import-cfs.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -26,10 +27,16 @@ my $importer = RT::LDAPImport->new;
 isa_ok($importer,'RT::LDAPImport');
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port");
 
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 $ldap->add("ou=foo,dc=bestpractical,dc=com");
 
diff --git a/t/ldapimport/user-import-privileged.t b/t/ldapimport/user-import-privileged.t
index 4b155eea..28ee9463 100644
--- a/t/ldapimport/user-import-privileged.t
+++ b/t/ldapimport/user-import-privileged.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -11,10 +12,16 @@ my $importer = RT::LDAPImport->new;
 isa_ok($importer,'RT::LDAPImport');
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port");
 
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 $ldap->add("ou=foo,dc=bestpractical,dc=com");
 
diff --git a/t/ldapimport/user-import.t b/t/ldapimport/user-import.t
index c4f6a593..7b8ab2d1 100644
--- a/t/ldapimport/user-import.t
+++ b/t/ldapimport/user-import.t
@@ -1,5 +1,6 @@
 use strict;
 use warnings;
+use IO::Socket::INET;
 
 use RT::Test tests => undef;
 
@@ -11,10 +12,16 @@ my $importer = RT::LDAPImport->new;
 isa_ok($importer,'RT::LDAPImport');
 
 my $ldap_port = RT::Test->find_idle_port;
-ok( my $server = Net::LDAP::Server::Test->new( $ldap_port, auto_schema => 1 ),
+my $ldap_socket = IO::Socket::INET->new(
+    Listen    => 5,
+    Proto     => 'tcp',
+    Reuse     => 1,
+    LocalPort => $ldap_port,
+);
+ok( my $server = Net::LDAP::Server::Test->new( $ldap_socket, auto_schema => 1 ),
     "spawned test LDAP server on port $ldap_port");
 
-my $ldap = Net::LDAP->new("localhost:$ldap_port");
+my $ldap = Net::LDAP->new("localhost:$ldap_port") || die "Failed to connect to LDAP server: $@";
 $ldap->bind();
 $ldap->add("ou=foo,dc=bestpractical,dc=com");
 
