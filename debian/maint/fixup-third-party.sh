#!/bin/bash

set -e

RTVER=4.4.7
THIRD_PARTY_TARBALL_DIR=../../../rt-$RTVER/devel/third-party
SRC=$THIRD_PARTY_TARBALL_DIR/ckeditor-src
SKINSRC=$THIRD_PARTY_TARBALL_DIR/flat-src
SCAYTSRC=$THIRD_PARTY_TARBALL_DIR/scayt-src
WSCSRC=$THIRD_PARTY_TARBALL_DIR/wsc-src
PLUGIN_LIST=ckeditor-plugins.txt

if [ -z "$5" ]; then
    echo "Usage: $0 <path-to-clean-checkout-of-ckeditor> <path to flat skin> <path-to-clean-checkout-of-scayt> <path-to-clean-checkout-of-wsc> <version of ckeditor to use>"
    exit 1
fi

CHECKOUT=$1      # git@github.com:ckeditor/ckeditor4.git
SKINCHECKOUT=$2  # https://ckeditor.com/cke4/addon/flat
SCAYTCHECKOUT=$3 # https://github.com/WebSpellChecker/ckeditor-plugin-scayt
WSCCHECKOUT=$4   # https://github.com/WebSpellChecker/ckeditor-plugin-wsc
VER=$5

## Prepare sources

(
    cd $CHECKOUT
    git checkout $VER
)

rm -rf $SRC
cp -a $CHECKOUT/ $SRC/
rm -rf $SRC/.git

rm -rf $SKINSRC
cp -a $SKINCHECKOUT/ $SKINSRC/

(
    cd $CHECKOUT
    git checkout master
)

cp -a $CHECKOUT/plugins/textmatch $CHECKOUT/plugins/selectall $SRC/plugins

rm -rf $SCAYTSRC
cp -a $SCAYTCHECKOUT/ $SCAYTSRC/
rm -rf $SCAYTSRC/.git

rm -rf $WSCSRC
cp -a $WSCCHECKOUT/ $WSCSRC/
rm -rf $WSCSRC/.git

## Tidy up sources

# $PLUGIN_LIST is manually curated - it includes deps not listed in 
# build-config.js
comm -1 -3 <(sort $PLUGIN_LIST) <(ls -1 $SRC/plugins/|sort ) | while read p; do
    rm -rf $SRC/plugins/$p
done
rm -rf $SRC/{skins,tests}
